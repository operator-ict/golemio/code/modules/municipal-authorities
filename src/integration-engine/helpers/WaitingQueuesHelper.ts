import { IInputQueue, ISkodaPalaceQueuesInput } from "#sch/datasources/SkodaPalaceQueuesJsonSchema";
import { IInputQueueForTransform } from "#sch/SkodaPalaceQueues";

export class WaitingQueuesHelper {
    public getActiveQueues(data: ISkodaPalaceQueuesInput): IInputQueue[] {
        return data.return.filter((item) => item.m_aktivni === 1);
    }

    public getLastUpdatedDate(data: ISkodaPalaceQueuesInput): Date {
        const lastUpdated = data.apiInfo.currentTime * 1000;
        return new Date(lastUpdated);
    }

    public getInputQueuesForTransform(inputQueues: IInputQueue[], lastUpdated: Date): IInputQueueForTransform[] {
        return inputQueues.map((inputQueue) => ({ inputQueue, lastUpdated }));
    }

    public getActiveInputQueuesForTransform(data: ISkodaPalaceQueuesInput): IInputQueueForTransform[] {
        const activeQueues = this.getActiveQueues(data);
        const lastUpdatedDate = this.getLastUpdatedDate(data);
        return this.getInputQueuesForTransform(activeQueues, lastUpdatedDate);
    }
}
