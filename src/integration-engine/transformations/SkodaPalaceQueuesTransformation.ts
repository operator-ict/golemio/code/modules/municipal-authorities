import { IInputQueueForTransform, ISkodaPalaceQueue } from "#sch/SkodaPalaceQueues";
import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";

export class SkodaPalaceQueuesTransformation extends AbstractTransformation<IInputQueueForTransform, ISkodaPalaceQueue> {
    public name = "SkodaPalaceQueuesTransformation";

    protected transformInternal = (element: IInputQueueForTransform): ISkodaPalaceQueue => {
        return {
            last_updated: element.lastUpdated,
            municipal_authority_id: "skoduv-palac",
            activity: element.inputQueue.nazev,
            number_of_person_in_queue: element.inputQueue.fronta,
            number_of_serving_counters: element.inputQueue.pocet_prepazek,
            title: "Monitoring odbavování klientů ve Škodově paláci",
        };
    };
}
