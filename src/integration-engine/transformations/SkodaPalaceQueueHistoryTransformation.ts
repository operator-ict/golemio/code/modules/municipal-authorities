import { AbstractTransformation } from "@golemio/core/dist/helpers/transformation/AbstractTransformation";
import { ISkodaPalaceQueueHistory, ISkodaPalaceQueueMessage } from "#sch/SkodaPalaceQueues";

export class SkodaPalaceQueueHistoryTransformation extends AbstractTransformation<
    ISkodaPalaceQueueMessage,
    ISkodaPalaceQueueHistory
> {
    name: string = "SkodaPalaceQueueHistoryTransformation";

    protected transformInternal = (message: ISkodaPalaceQueueMessage): ISkodaPalaceQueueHistory => {
        return {
            activity: message.activity,
            last_updated: new Date(message.last_updated),
            municipal_authority_id: message.municipal_authority_id,
            number_of_person_in_queue: message.number_of_person_in_queue,
            number_of_serving_counters: message.number_of_serving_counters,
        };
    };
}
