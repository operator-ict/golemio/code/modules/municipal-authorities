import { config } from "@golemio/core/dist/integration-engine/config";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { MunicipalAuthorities } from "#sch";
import { IAgenda, IMunicipalAuthority, IOpeningHour } from "#sch/MunicipalAuthorities";
import { IMunicipalAuthoritiesInput } from "#sch/datasources/MunicipalAuthoritiesJsonSchema";

enum openingHoursDaysKeys {
    opening_hours_monday = "Monday",
    opening_hours_tuesday = "Tuesday",
    opening_hours_wednesday = "Wednesday",
    opening_hours_thursday = "Thursday",
    opening_hours_friday = "Friday",
    opening_hours_saturday = "Saturday",
    opening_hours_sunday = "Sunday",
}

export class MunicipalAuthoritiesTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MunicipalAuthorities.name;
    }

    public transform = async (data: IMunicipalAuthoritiesInput[]): Promise<IMunicipalAuthority[]> => {
        const authorities: IMunicipalAuthority[] = [];

        for (const item of data) {
            const transformedData = await this.transformElement(item);
            authorities.push(transformedData);
        }

        return authorities;
    };

    protected transformElement = async (element: IMunicipalAuthoritiesInput): Promise<IMunicipalAuthority> => {
        const res: IMunicipalAuthority = {
            id: element.slug,
            geometry: {
                coordinates: element.coordinates,
                type: "Point",
            },
            name: element.name,
            image: {
                mimetype: null,
                size: null,
                url: null,
            },
        };

        if (element.address) {
            res.address_formatted = element.address;
        }

        if (element.telephone) {
            res.telephone = element.telephone;
        }
        if (element.web) {
            res.web = element.web;
        }

        if (element.email) {
            res.email = element.email;
        }
        if (element.district) {
            res.district = element.district;
        }

        if (element.official_board) {
            res.official_board = element.official_board;
        }

        if (element.image?.mimetype) {
            res.image.mimetype = element.image.mimetype;
        }
        if (element.image?.size) {
            res.image.size = element.image.size;
        }
        if (element.image?.url) {
            res.image.url = config.MOJEPRAHA_ENDPOINT_BASEURL + element.image.url;
        }

        if (element.type) {
            res.type = element.type;
            res.description = this.getTypeDescription(element.type);
        }

        let opening_hours: IOpeningHour[] = [];
        Object.keys(openingHoursDaysKeys).map((openingHoursDaysKey) => {
            if (
                element[openingHoursDaysKey as keyof typeof openingHoursDaysKeys] &&
                element[openingHoursDaysKey as keyof typeof openingHoursDaysKeys] !== ""
            ) {
                opening_hours = [
                    ...opening_hours,
                    // @ts-ignore
                    ...this.transformOpeningHours(openingHoursDaysKeys[openingHoursDaysKey], element[openingHoursDaysKey]),
                ];
            }
        });
        if (opening_hours) {
            res.opening_hours = opening_hours;
        }
        if (element.agendas) {
            const agendas = element.agendas.map((agenda) => {
                const newAgenda: IAgenda = {};
                newAgenda.keywords = agenda.keywords ? agenda.keywords : [];
                if (agenda.long_description) {
                    newAgenda.long_description = agenda.long_description;
                }
                if (agenda.description) {
                    newAgenda.description = agenda.description;
                }
                return newAgenda;
            });
            res.agendas = agendas;
        }

        return res;
    };

    private getTypeDescription = (id: string): string => {
        switch (id) {
            case "city-hall":
                return "Magistrát";
            case "municipality":
                return "Obecní úřad";
            default:
                return "";
        }
    };

    private transformOpeningHours(dayString: string, openingHoursString: string): IOpeningHour[] {
        const res: IOpeningHour[] = [];
        openingHoursString.split("; ").forEach((hoursByScope: string) => {
            const desc = hoursByScope.match(/\(([^)]+)\)/);
            hoursByScope
                .replace(/\(([^)]+)\)/, "")
                .split(", ")
                .forEach((hours: string) => {
                    const [opens, closes]: string[] = hours.split(" - ");
                    res.push({
                        closes: closes.trim(),
                        day_of_week: dayString,
                        description: desc && desc[1] ? desc[1] : null,
                        opens: opens.trim(),
                    });
                });
        });
        return res;
    }
}
