import { ISkodaPalaceQueueMessage } from "#sch/SkodaPalaceQueues";
import { IsDateString, IsNumber, IsString } from "@golemio/core/dist/shared/class-validator";

export class SaveWaitingQueuesDataToHistorySchema implements ISkodaPalaceQueueMessage {
    @IsString()
    @IsDateString()
    last_updated!: string;
    @IsString()
    municipal_authority_id!: string;
    @IsString()
    activity!: string;
    @IsNumber()
    number_of_person_in_queue!: number;
    @IsNumber()
    number_of_serving_counters!: number;
    @IsString()
    title!: string;
}
