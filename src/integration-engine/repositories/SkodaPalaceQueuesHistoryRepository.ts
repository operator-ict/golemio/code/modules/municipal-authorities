import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

import { MunicipalAuthorities } from "#sch";
import { skodaPalaceQueuesHistory } from "#sch/SkodaPalaceQueues";
import { QueueHistoryModel } from "#sch/models/QueueHistoryModel";

export class SkodaPalaceQueuesHistoryRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "SkodaPalaceQueuesHistoryRepository",
            {
                outputSequelizeAttributes: QueueHistoryModel.attributeModel,
                pgTableName: skodaPalaceQueuesHistory.pgTableName,
                pgSchema: MunicipalAuthorities.pgSchema,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator("SkodaPalaceQueuesHistoryRepositoryValidator", QueueHistoryModel.jsonSchema)
        );
    }
}
