import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MunicipalAuthorities } from "#sch";
import { IMunicipalAuthority } from "#sch/MunicipalAuthorities";
import { AuthorityModel } from "#sch/models/AuthorityModel";

export class MunicipalAuthoritiesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "MunicipalAuthoritiesRepository",
            {
                outputSequelizeAttributes: AuthorityModel.attributeModel,
                pgTableName: MunicipalAuthorities.definitions.municipalAuthorities.pgTableName,
                pgSchema: MunicipalAuthorities.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("MunicipalAuthoritiesRepositoryValidator", AuthorityModel.jsonSchema)
        );
    }

    public saveBulk = async (data: IMunicipalAuthority[]) => {
        if (await this.validate(data)) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => el !== "id")
                .map<keyof IMunicipalAuthority>((el) => el as keyof IMunicipalAuthority);
            fieldsToUpdate.push("updated_at" as any);
            await this.sequelizeModel.bulkCreate<AuthorityModel>(data, {
                updateOnDuplicate: fieldsToUpdate,
            });
        }
    };
}
