/* ie/repositories/index.ts */
export * from "./MunicipalAuthoritiesRepository";
export * from "./SkodaPalaceQueuesRepository";
export * from "./SkodaPalaceQueuesHistoryRepository";
