import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

import { MunicipalAuthorities } from "#sch";
import { ISkodaPalaceQueue, skodaPalaceQueues } from "#sch/SkodaPalaceQueues";
import { QueueModel } from "#sch/models/QueueModel";

export class SkodaPalaceQueuesRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "SkodaPalaceQueuesRepository",
            {
                outputSequelizeAttributes: QueueModel.attributeModel,
                pgTableName: skodaPalaceQueues.pgTableName,
                pgSchema: MunicipalAuthorities.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("SkodaPalaceQueuesRepositoryValidator", QueueModel.jsonSchema)
        );
    }

    public saveBulk = async (data: ISkodaPalaceQueue[]) => {
        if (await this.validate(data)) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => el !== "id")
                .map<keyof ISkodaPalaceQueue>((el) => el as keyof ISkodaPalaceQueue);
            fieldsToUpdate.push("updated_at" as any);
            await this.sequelizeModel.bulkCreate<QueueModel>(data, {
                updateOnDuplicate: fieldsToUpdate,
            });
        }
    };
}
