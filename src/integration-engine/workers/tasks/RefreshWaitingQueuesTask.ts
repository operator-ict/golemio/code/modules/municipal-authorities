import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { SkodaPalaceQueuesDataSourceFactory } from "#ie/datasources/SkodaPalaceQueuesDataSource";
import { SkodaPalaceQueuesTransformation } from "#ie/transformations/SkodaPalaceQueuesTransformation";
import { SkodaPalaceQueuesRepository } from "#ie/repositories";
import { WaitingQueuesHelper } from "#ie/helpers/WaitingQueuesHelper";

export class RefreshWaitingQueuesTask extends AbstractEmptyTask {
    public readonly queueName = "refreshWaitingQueues";
    public readonly queueTtl = 60 * 1000;

    private dataSource: DataSource;
    private transformation: SkodaPalaceQueuesTransformation;
    private repository: SkodaPalaceQueuesRepository;
    private helper: WaitingQueuesHelper;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = SkodaPalaceQueuesDataSourceFactory.getDataSource();
        this.transformation = new SkodaPalaceQueuesTransformation();
        this.repository = new SkodaPalaceQueuesRepository();
        this.helper = new WaitingQueuesHelper();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSource.getAll();
        const inputQueuesForTransform = this.helper.getActiveInputQueuesForTransform(data);
        const transformedData = this.transformation.transformArray(inputQueuesForTransform);

        await this.repository.saveBulk(transformedData);

        const promises = transformedData.map((queueObj) => {
            return QueueManager.sendMessageToExchange(this.queuePrefix, "saveWaitingQueuesDataToHistory", queueObj);
        });
        await Promise.all(promises);
    }
}
