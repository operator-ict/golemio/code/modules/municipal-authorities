import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { MunicipalAuthoritiesDataSourceFactory } from "#ie/datasources/MunicipalAuthoritiesDataSource";
import { MunicipalAuthoritiesTransformation } from "#ie/transformations/MunicipalAuthoritiesTransformation";
import { MunicipalAuthoritiesRepository } from "#ie/repositories";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours

    private dataSource: DataSource;
    private transformation: MunicipalAuthoritiesTransformation;
    private repository: MunicipalAuthoritiesRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = MunicipalAuthoritiesDataSourceFactory.getDataSource();
        this.transformation = new MunicipalAuthoritiesTransformation();
        this.repository = new MunicipalAuthoritiesRepository();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSource.getAll();
        const transformedData = await this.transformation.transform(data);
        await this.repository.saveBulk(transformedData);
    }
}
