import { SkodaPalaceQueuesHistoryRepository } from "#ie/repositories";
import { SaveWaitingQueuesDataToHistorySchema } from "#ie/schema/SaveWaitingQueuesDataToHistorySchema";
import { ISkodaPalaceQueueMessage } from "#sch/SkodaPalaceQueues";
import { AbstractTask } from "@golemio/core/dist/integration-engine";
import { SkodaPalaceQueueHistoryTransformation } from "#ie/transformations/SkodaPalaceQueueHistoryTransformation";

export class SaveWaitingQueuesDataToHistoryTask extends AbstractTask<ISkodaPalaceQueueMessage> {
    public readonly queueName = "saveWaitingQueuesDataToHistory";
    public readonly schema = SaveWaitingQueuesDataToHistorySchema;
    private transformation: SkodaPalaceQueueHistoryTransformation;
    private repository: SkodaPalaceQueuesHistoryRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.transformation = new SkodaPalaceQueueHistoryTransformation();
        this.repository = new SkodaPalaceQueuesHistoryRepository();
    }

    protected async execute(msg: ISkodaPalaceQueueMessage): Promise<void> {
        const transformedData = this.transformation.transformElement(msg);
        await this.repository.save(transformedData);
    }
}
