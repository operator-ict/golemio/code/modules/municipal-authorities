import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshDataInDBTask, RefreshWaitingQueuesTask, SaveWaitingQueuesDataToHistoryTask } from "./tasks";

export class MunicipalAuthoritiesWorker extends AbstractWorker {
    protected readonly name = "MunicipalAuthorities";

    constructor() {
        super();
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
        this.registerTask(new RefreshWaitingQueuesTask(this.getQueuePrefix()));
        this.registerTask(new SaveWaitingQueuesDataToHistoryTask(this.getQueuePrefix()));
    }
}
