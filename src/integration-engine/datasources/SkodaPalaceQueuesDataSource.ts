import { MunicipalAuthorities } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class SkodaPalaceQueuesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            MunicipalAuthorities.datasources.skodaPalaceQueuesDatasource.name,
            new HTTPFetchProtocolStrategy({
                body: JSON.stringify({
                    methodName: "mon_dej_cinnost",
                    params: {
                        poboID: 1,
                        apiKey: config.datasources.SkodaPalaceQueuesApiKey,
                    },
                }),
                headers: { Authorization: "Basic " + config.datasources.OICTEndpointApikey },
                method: "POST",
                url: config.datasources.SkodaPalaceQueues,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                MunicipalAuthorities.datasources.skodaPalaceQueuesDatasource.name + "Validator",
                MunicipalAuthorities.datasources.skodaPalaceQueuesDatasource.jsonSchema,
                true
            )
        );
    }
}
