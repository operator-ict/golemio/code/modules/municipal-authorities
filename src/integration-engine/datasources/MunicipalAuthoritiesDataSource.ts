import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource, JSONDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MunicipalAuthorities } from "src/schema-definitions";

export class MunicipalAuthoritiesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            MunicipalAuthorities.datasources.municipalAuthoritiesDatasource.name,
            new HTTPFetchProtocolStrategy({
                headers: { Authorization: "Basic " + config.datasources.OICTEndpointApikey },
                method: "GET",
                url: config.datasources.MunicipalAuthorities,
            }),
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(
                MunicipalAuthorities.datasources.municipalAuthoritiesDatasource.name + "Validator",
                MunicipalAuthorities.datasources.municipalAuthoritiesDatasource.jsonSchema,
                true
            )
        );
    }
}
