import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISkodaPalaceQueue } from "#sch/SkodaPalaceQueues";

export class QueueModel extends Model<ISkodaPalaceQueue> implements ISkodaPalaceQueue {
    declare last_updated: Date;
    declare municipal_authority_id: string;
    declare activity: string;
    declare number_of_person_in_queue: number;
    declare number_of_serving_counters: number;
    declare title: string;
    declare updated_at?: string;

    public static attributeModel: ModelAttributes<QueueModel, ISkodaPalaceQueue> = {
        municipal_authority_id: { type: DataTypes.STRING },
        last_updated: { type: DataTypes.DATE },
        activity: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        number_of_person_in_queue: { type: DataTypes.INTEGER },
        number_of_serving_counters: { type: DataTypes.INTEGER },
        title: { type: DataTypes.STRING },
        updated_at: { type: DataTypes.STRING },
    };

    public static jsonSchema: JSONSchemaType<ISkodaPalaceQueue[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                municipal_authority_id: { type: "string" },
                last_updated: { type: "object", required: ["toISOString"] },
                activity: { type: "string" },
                number_of_person_in_queue: { type: "number" },
                number_of_serving_counters: { type: "number" },
                title: { type: "string" },
                updated_at: { type: "string", nullable: true },
            },
            required: [
                "municipal_authority_id",
                "last_updated",
                "activity",
                "number_of_person_in_queue",
                "number_of_serving_counters",
                "title",
            ],
        },
    };
}
