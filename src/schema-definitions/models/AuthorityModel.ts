import { Point } from "geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { IAgenda, IImage, IMunicipalAuthority, IOpeningHour } from "#sch/MunicipalAuthorities";

export class AuthorityModel extends Model<IMunicipalAuthority> implements IMunicipalAuthority {
    declare id: string;
    declare geometry: Point;
    declare address_formatted?: string;
    declare agendas?: IAgenda[];
    declare district?: string;
    declare email?: string[];
    declare image: IImage;
    declare name: string;
    declare official_board?: string;
    declare opening_hours?: IOpeningHour[];
    declare telephone?: string[];
    declare type?: string;
    declare description?: string;
    declare web?: string[];

    public static attributeModel: ModelAttributes<AuthorityModel, IMunicipalAuthority> = {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        geometry: DataTypes.GEOMETRY,
        address_formatted: { type: DataTypes.STRING },
        agendas: { type: DataTypes.JSON },
        district: { type: DataTypes.STRING },
        email: { type: DataTypes.JSON },
        image: { type: DataTypes.JSON },
        name: { type: DataTypes.STRING },
        official_board: { type: DataTypes.STRING },
        opening_hours: { type: DataTypes.JSON },
        telephone: { type: DataTypes.JSON },
        type: { type: DataTypes.STRING },
        description: { type: DataTypes.STRING },
        web: { type: DataTypes.JSON },
    };

    public static jsonSchema: JSONSchemaType<IMunicipalAuthority[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                geometry: { $ref: "#/definitions/geometry" },
                address_formatted: { type: "string" },
                agendas: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            description: { type: "string" },
                            keywords: { type: "array", items: { type: "string" } },
                            long_description: { type: "string" },
                        },
                    },
                },
                district: { type: "string" },
                email: {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                image: {
                    type: "object",
                    properties: {
                        mimetype: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                        size: { oneOf: [{ type: "number" }, { type: "null", nullable: true }] },
                        url: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    },
                },
                name: { type: "string" },
                official_board: { type: "string" },
                opening_hours: {
                    type: "array",
                    items: {
                        type: "object",
                        properties: {
                            closes: { type: "string" },
                            day_of_week: { type: "string" },
                            description: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                            opens: { type: "string" },
                        },
                        required: ["closes", "day_of_week", "description", "opens"],
                    },
                },
                telephone: {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
                type: { type: "string" },
                description: { type: "string" },
                web: {
                    type: "array",
                    items: {
                        type: "string",
                    },
                },
            },
            required: ["id", "geometry", "name"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
