import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { ISkodaPalaceQueueHistory } from "#sch/SkodaPalaceQueues";

export class QueueHistoryModel extends Model<ISkodaPalaceQueueHistory> implements ISkodaPalaceQueueHistory {
    declare last_updated: Date;
    declare municipal_authority_id: string;
    declare activity: string;
    declare number_of_person_in_queue: number;
    declare number_of_serving_counters: number;
    declare updated_at?: string;

    public static attributeModel: ModelAttributes<QueueHistoryModel, ISkodaPalaceQueueHistory> = {
        municipal_authority_id: { type: DataTypes.STRING },
        last_updated: { type: DataTypes.DATE, primaryKey: true },
        activity: { type: DataTypes.STRING, primaryKey: true },
        number_of_person_in_queue: { type: DataTypes.NUMBER },
        number_of_serving_counters: { type: DataTypes.NUMBER },
        updated_at: { type: DataTypes.DATE },
    };

    public static jsonSchema: JSONSchemaType<ISkodaPalaceQueueHistory> = {
        type: "object",
        properties: {
            municipal_authority_id: { type: "string" },
            last_updated: { type: "object", required: ["toISOString"] },
            activity: { type: "string" },
            number_of_person_in_queue: { type: "number" },
            number_of_serving_counters: { type: "number" },
            updated_at: { type: "string", nullable: true },
        },
        required: [
            "municipal_authority_id",
            "last_updated",
            "activity",
            "number_of_person_in_queue",
            "number_of_serving_counters",
        ],
    };
}
