import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IAgenda {
    description?: string;
    keywords?: string[];
    long_description?: string;
}

export interface IInputQueue {
    cinnostID: number;
    nazev: string;
    fronta: number;
    pocet_prepazek: number;
    doba_cekani_odhad?: number;
    m_poradi_aktivni?: number;
    m_aktivni: number;
    doba_cekani?: number;
}

export interface ISkodaPalaceQueuesInput {
    return: IInputQueue[];
    apiInfo: {
        currentTime: number;
        apiVersion?: string;
        execTime?: number;
        usageMemory?: number;
    };
}

const skodaPalaceQueuesJsonSchema: JSONSchemaType<ISkodaPalaceQueuesInput> = {
    type: "object",
    properties: {
        return: {
            type: "array",
            items: {
                type: "object",
                properties: {
                    cinnostID: {
                        type: "number",
                    },
                    nazev: {
                        type: "string",
                    },
                    fronta: {
                        type: "number",
                    },
                    pocet_prepazek: {
                        type: "number",
                    },
                    doba_cekani_odhad: { type: "number", nullable: true },
                    m_poradi_aktivni: { type: "number", nullable: true },
                    m_aktivni: { type: "number" },
                    doba_cekani: { type: "number", nullable: true },
                },
                required: ["cinnostID", "nazev", "m_aktivni", "fronta", "pocet_prepazek"],
            },
        },
        apiInfo: {
            type: "object",
            properties: {
                currentTime: { type: "number" },
                apiVersion: { type: "string", nullable: true },
                execTime: { type: "number", nullable: true },
                usageMemory: { type: "number", nullable: true },
            },
            required: ["currentTime"],
        },
    },
    required: ["return", "apiInfo"],
};

export const skodaPalaceQueuesDatasource: { name: string; jsonSchema: JSONSchemaType<ISkodaPalaceQueuesInput> } = {
    name: "SkodaPalaceQueuesDatasource",
    jsonSchema: skodaPalaceQueuesJsonSchema,
};
