import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IAgenda {
    description?: string;
    keywords?: string[];
    long_description?: string;
}

export interface IImage {
    filename?: string;
    size?: number;
    mimetype?: string;
    url?: string;
}

export interface IMunicipalAuthoritiesInput {
    name: string;
    slug: string;
    address?: string;
    district?: string;
    coordinates: number[];
    opening_hours_friday?: string;
    opening_hours_monday?: string;
    opening_hours_saturday?: string;
    opening_hours_sunday?: string;
    opening_hours_thursday?: string;
    opening_hours_tuesday?: string;
    opening_hours_wednesday?: string;
    official_board?: string;
    agendas?: IAgenda[];
    image?: IImage;
    type?: string;
    web?: string[];
    email?: string[];
    telephone?: string[];
}

const municipalAuthoritiesJsonSchema: JSONSchemaType<IMunicipalAuthoritiesInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            name: { type: "string" },
            slug: { type: "string" },
            address: { type: "string", nullable: true },
            district: { type: "string", nullable: true },
            coordinates: {
                type: "array",
                items: {
                    type: "number",
                },
                minItems: 2,
                maxItems: 2,
            },
            opening_hours_friday: { type: "string", nullable: true },
            opening_hours_monday: { type: "string", nullable: true },
            opening_hours_saturday: { type: "string", nullable: true },
            opening_hours_sunday: { type: "string", nullable: true },
            opening_hours_thursday: { type: "string", nullable: true },
            opening_hours_tuesday: { type: "string", nullable: true },
            opening_hours_wednesday: { type: "string", nullable: true },
            official_board: { type: "string", nullable: true },
            agendas: {
                type: "array",
                items: {
                    type: "object",
                    properties: {
                        description: { type: "string", nullable: true },
                        keywords: {
                            type: "array",
                            items: { type: "string" },
                            nullable: true,
                        },
                        long_description: { type: "string", nullable: true },
                    },
                },
                nullable: true,
            },
            image: {
                type: "object",
                properties: {
                    filename: { type: "string", nullable: true },
                    size: { type: "number", nullable: true },
                    mimetype: { type: "string", nullable: true },
                    url: { type: "string", nullable: true },
                },
                nullable: true,
            },
            type: { type: "string", nullable: true },
            web: {
                type: "array",
                items: {
                    type: "string",
                },
                nullable: true,
            },
            email: {
                type: "array",
                items: {
                    type: "string",
                },
                nullable: true,
            },
            telephone: {
                type: "array",
                items: {
                    type: "string",
                },
                nullable: true,
            },
        },
        required: ["name", "slug", "coordinates"],
        additionalProperties: false,
    },
};

export const municipalAuthoritiesDatasource: { name: string; jsonSchema: JSONSchemaType<IMunicipalAuthoritiesInput[]> } = {
    name: "MunicipalAuthoritiesDatasource",
    jsonSchema: municipalAuthoritiesJsonSchema,
};
