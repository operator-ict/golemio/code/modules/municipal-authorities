import { IInputQueue } from "#sch/datasources/SkodaPalaceQueuesJsonSchema";

export interface IInputQueueForTransform {
    lastUpdated: Date;
    inputQueue: IInputQueue;
}

export interface ISkodaPalaceQueue extends ISkodaPalaceQueueHistory {
    title: string;
}

export interface ISkodaPalaceQueueMessage {
    last_updated: string;
    municipal_authority_id: string;
    activity: string;
    number_of_person_in_queue: number;
    number_of_serving_counters: number;
    title: string;
    updated_at?: string;
}

export interface ISkodaPalaceQueueHistory {
    last_updated: Date;
    municipal_authority_id: string;
    activity: string;
    number_of_person_in_queue: number;
    number_of_serving_counters: number;
    updated_at?: string;
}

export interface IServedActivity {
    activity: string;
    number_of_person_in_queue: number;
    number_of_serving_counters: number;
}

export interface ISkodaPalaceQueueOutput {
    last_updated: string | null;
    municipal_authority_id: string;
    served_activities: IServedActivity[];
    updated_at: string | null;
    title: string;
}

export const skodaPalaceQueues = {
    name: "SkodaPalaceQueues",
    pgTableName: "skodapalace_queues",
};

export const skodaPalaceQueuesHistory = {
    name: "SkodaPalaceQueuesHistory",
    pgTableName: "skodapalace_queues_history",
};
