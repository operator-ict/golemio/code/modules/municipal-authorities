import { Point } from "geojson";

export interface IAgenda {
    description?: string;
    keywords?: string[];
    long_description?: string;
}

export interface IImage {
    mimetype: string | null;
    size: number | null;
    url: string | null;
}

export interface IOpeningHour {
    closes: string;
    day_of_week: string;
    description: string | null;
    opens: string;
}

export interface IMunicipalAuthority {
    id: string;
    geometry: Point;
    address_formatted?: string;
    agendas?: IAgenda[];
    district?: string;
    email?: string[];
    image: IImage;
    name: string;
    official_board?: string;
    opening_hours?: IOpeningHour[];
    telephone?: string[];
    type?: string;
    description?: string;
    web?: string[];
}

export const municipalAuthorities = {
    name: "MunicipalAuthorities",
    pgTableName: "municipalauthorities",
};
