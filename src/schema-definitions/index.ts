import { municipalAuthoritiesDatasource } from "#sch/datasources/MunicipalAuthoritiesJsonSchema";
import { skodaPalaceQueuesDatasource } from "#sch/datasources/SkodaPalaceQueuesJsonSchema";
import { municipalAuthorities } from "#sch/MunicipalAuthorities";

const forExport: any = {
    name: "MunicipalAuthorities",
    pgSchema: "municipal_authorities",
    datasources: {
        municipalAuthoritiesDatasource,
        skodaPalaceQueuesDatasource,
    },
    definitions: {
        municipalAuthorities,
    },
};

export { forExport as MunicipalAuthorities };
