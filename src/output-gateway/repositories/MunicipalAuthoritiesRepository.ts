import Sequelize from "@golemio/core/dist/shared/sequelize";
import { IPropertyResponseModel, SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureLatLng,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
} from "@golemio/core/dist/output-gateway";
import { FilterHelper } from "./FilterHelper";

import { MunicipalAuthorities } from "#sch";
import { AuthorityModel } from "#sch/models/AuthorityModel";

export class MunicipalAuthoritiesRepository extends SequelizeModel implements IGeoJsonModel {
    constructor() {
        super(
            "MunicipalAuthoritiesRepository",
            MunicipalAuthorities.definitions.municipalAuthorities.pgTableName,
            AuthorityModel.attributeModel,
            { schema: MunicipalAuthorities.pgSchema }
        );
    }

    public IsPrimaryIdNumber(idKey: string): Promise<boolean> {
        return Promise.resolve(false);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetAll = async (options: IGeoJsonAllFilterParameters = {}): Promise<IGeoJSONFeatureCollection> => {
        const result = await this.sequelizeModel.findAll<AuthorityModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: options
                ? {
                      [Sequelize.Op.and]: [
                          ...FilterHelper.prepareFilterForType(options.additionalFilters),
                          ...FilterHelper.prepareFilterForLocation(options),
                          ...FilterHelper.prepareFilterForUpdateSince(options),
                          ...FilterHelper.prepareFilterForDistricts(options),
                      ],
                  }
                : {},
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order: FilterHelper.prepareOrderFunction(options),
        });

        return buildGeojsonFeatureCollection(
            result.map((record: AuthorityModel) => {
                return this.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string): Promise<IGeoJSONFeature | undefined> => {
        const result = await this.sequelizeModel.findOne<AuthorityModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: { id },
        });

        return result ? this.formatOutput(result) : undefined;
    };

    private formatOutput = (record: AuthorityModel): IGeoJSONFeature | undefined => {
        const { geometry, address_formatted, type, description, ...rest } = record.get({ plain: true });
        return buildGeojsonFeatureLatLng(
            {
                ...rest,
                ...{
                    address: {
                        address_formatted,
                    },
                    type: {
                        description,
                        id: type,
                    },
                },
            },
            geometry.coordinates[0],
            geometry.coordinates[1]
        );
    };
}
