import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { MunicipalAuthorities } from "#sch";
import { QueueModel } from "#sch/models/QueueModel";
import { ISkodaPalaceQueueOutput, skodaPalaceQueues } from "#sch/SkodaPalaceQueues";
import { DateTime } from "@golemio/core/dist/shared/luxon";

export class SkodaPalaceQueuesRepository extends SequelizeModel {
    constructor() {
        super("SkodaPalaceQueuesRepository", skodaPalaceQueues.pgTableName, QueueModel.attributeModel, {
            schema: MunicipalAuthorities.pgSchema,
        });
    }

    public GetAll = async (): Promise<ISkodaPalaceQueueOutput | null> => {
        const result = await this.sequelizeModel.findAll<QueueModel>({
            attributes: {
                include: ["updated_at"],
            },
            raw: true,
        });
        if (result && result.length) {
            return {
                last_updated: result[0].last_updated
                    ? DateTime.fromJSDate(new Date(+result[0].last_updated), { zone: "Europe/Prague" }).toISO()
                    : null,
                municipal_authority_id: "skoduv-palac",
                served_activities: result.map((element) => {
                    return {
                        activity: element.activity,
                        number_of_person_in_queue: +element.number_of_person_in_queue,
                        number_of_serving_counters: +element.number_of_serving_counters,
                    };
                }),
                updated_at: result[0].updated_at
                    ? DateTime.fromJSDate(new Date(result[0].updated_at), { zone: "Europe/Prague" }).toISO()
                    : null,
                title: "Monitoring odbavování klientů ve Škodově paláci",
            };
        }
        return null;
    };

    public GetOne = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };
}
