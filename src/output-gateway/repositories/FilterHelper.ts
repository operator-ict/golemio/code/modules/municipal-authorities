import { IGeoJsonAllFilterParameters } from "@golemio/core/dist/output-gateway";
import Sequelize, { WhereOptions } from "@golemio/core/dist/shared/sequelize";

export class FilterHelper {
    public static prepareFilterForType(additionalFiltersObj: { type?: string } | undefined): WhereOptions[] {
        return additionalFiltersObj && additionalFiltersObj.type && additionalFiltersObj.type.length > 0
            ? [
                  {
                      type: {
                          [Sequelize.Op.eq]: additionalFiltersObj.type,
                      },
                  },
              ]
            : [];
    }

    public static prepareFilterForDistricts(options: IGeoJsonAllFilterParameters): WhereOptions[] {
        return options.districts && options.districts.length > 0
            ? [
                  {
                      district: {
                          [Sequelize.Op.in]: options.districts,
                      },
                  },
              ]
            : [];
    }

    public static prepareFilterForUpdateSince(options: IGeoJsonAllFilterParameters): WhereOptions[] {
        return options.updatedSince
            ? [
                  {
                      updated_at: {
                          [Sequelize.Op.gt]: options?.updatedSince,
                      },
                  },
              ]
            : [];
    }

    public static prepareFilterForLocation(options: IGeoJsonAllFilterParameters): WhereOptions[] {
        return options.lat && options.lng && options.range
            ? [
                  Sequelize.fn(
                      "ST_DWithin",
                      Sequelize.col("geometry"),
                      Sequelize.cast(
                          Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                          "geography"
                      ),
                      options.range
                  ),
              ]
            : [];
    }

    public static prepareOrderFunction(options: IGeoJsonAllFilterParameters | undefined): Sequelize.Order | undefined {
        return options?.lat && options.lng
            ? [
                  Sequelize.fn(
                      "ST_Distance",
                      Sequelize.col("geometry"),
                      Sequelize.cast(
                          Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                          "geography"
                      )
                  ),
              ]
            : undefined;
    }
}
