import { checkErrors } from "@golemio/core/dist/output-gateway";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway/routes";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { param } from "@golemio/core/dist/shared/express-validator";
import { MunicipalAuthoritiesRepository } from ".";
import { SkodaPalaceQueuesRepository } from "./repositories/SkodaPalaceQueuesRepository";

const CACHE_MAX_AGE = 12 * 60 * 60;
const CACHE_STALE_WHILE_REVALIDATE = 60 * 60;

export class MunicipalAuthoritiesRouter extends GeoJsonRouter {
    public skodaPalaceQueuesRepository: SkodaPalaceQueuesRepository;
    constructor() {
        super(new MunicipalAuthoritiesRepository());
        this.skodaPalaceQueuesRepository = new SkodaPalaceQueuesRepository();
        this.initRoutes({ maxAge: CACHE_MAX_AGE, staleWhileRevalidate: CACHE_STALE_WHILE_REVALIDATE });
        this.router.get(
            "/:id/queues",
            [param("id").not().isEmpty({ ignore_whitespace: true })],
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(CACHE_MAX_AGE, CACHE_STALE_WHILE_REVALIDATE),
            this.GetQueues
        );
    }

    public GetQueues = async (req: Request, res: Response, next: NextFunction) => {
        try {
            if (req.params.id != "skoduv-palac") {
                return res.status(404).send();
            }

            const data = await this.skodaPalaceQueuesRepository.GetAll();

            if (data) {
                res.status(200).send(data);
            } else {
                res.status(404).send();
            }
        } catch (err) {
            next(err);
        }
    };
}

const municipalAuthoritiesRouter: Router = new MunicipalAuthoritiesRouter().router;

export { municipalAuthoritiesRouter };
