# Implementační dokumentace modulu _municipal-authorities_

## Záměr

Modul slouží k ukládání a poskytování informací o Municipal Authorities.

## Vstupní data

### Stahujeme data ze dvou zdrojů, zpracováváme je a ukládáme do samostatných tabulek.

#### _Protected endpoint at mojepraha.eu_

-   zdroj dat
    -   url: [config.datasources.MunicipalAuthorities](https://www.mojepraha.eu/api/municipal-authorities)
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [municipalAuthoritiesJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/blob/development/src/integration-engine/datasources/MunicipalAuthoritiesDataSource.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/blob/development/test/integration-engine/data/municipalauthorities-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.municipalauthorities.refreshDataInDB
            -   rabin `0 25 7 * * *`
            -   prod `0 20 2 * * 5`
-   název rabbitmq fronty
    -   dataplatform.municipalauthorities.refreshDataInDB

#### _Protected endpoint at prod.kadlecelektro.cz_

-   zdroj dat
    -   url: [config.datasources.SkodaPalaceQueues](https://prod.kadlecelektro.cz/webcall/v4_mhmp/master/json.php)
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [skodaPalaceQueuesJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/blob/development/src/integration-engine/datasources/SkodaPalaceQueuesDataSource.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/blob/development/test/integration-engine/data/skodapalacequeues-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.municipalauthorities.refreshWaitingQueues
            -   rabin `0 23 7,13,19,1 * * *`
            -   prod `0 */2 * * * *`
-   název rabbitmq fronty
    -   dataplatform.municipalauthorities.refreshWaitingQueues

### MunicipalAuthorities

#### _task: RefreshDataInDBTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.municipalauthorities.refreshDataInDB
    -   bez parametrů
-   datové zdroje
    -   dataSource mojepraha.eu
-   transformace
    -   [MunicipalAuthoritiesTransformation.ts](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/blob/development/src/integration-engine/transformations/MunicipalAuthoritiesTransformation.ts) - mapování pro `AuthorityModel`

#### _task: RefreshWaitingQueuesTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.municipalauthorities.RefreshWaitingQueuesTask
-   datové zdroje
    -   dataSource prod.kadlecelektro.cz
-   transformace
    -   [SkodaPalaceQueuesTransformation.ts](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/blob/development/src/integration-engine/transformations/SkodaPalaceQueuesTransformation.ts) - mapování pro `QueueModel`
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: dataplatform.municipalauthorities.saveWaitingQueuesDataToHistory

#### \_task: SaveWaitingQueuesDataToHistoryTask

Ukládá data do db pro historizaci.

-   vstupní rabbitmq fronta
    -   název: dataplatform.municipalauthorities.saveWaitingQueuesDataToHistory
-   datové zdroje
    -   task: RefreshWaitingQueuesTask
-   transformace
    -   SkodaPalaceQueueHistoryTransformation.ts
-   výstup:
    -   SkodaPalaceQueuesHistoryRepository, tabulka `skodapalace_queues_history`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   ![municipalauthorities er diagram](./assets/municipalauthorities_erd.png)

## Output API

`MunicipalAuthoritiesRouter` implementuje `GeoJsonRouter`.

### Obecné

-   OpenAPI v3 dokumentace
    -   [OpenAPI](./openapi.yaml)
-   api je veřejné
-   postman kolekce
    -   TBD
-   Asyncapi dokumentace RabbitMQ front
    -   [AsyncAPI](./asyncapi.yaml)

#### /municipalauthorities

-   zdrojové tabulky
    -   `municipalauthorities`
-   dodatečná transformace: Feature
-

#### /municipalauthorities/:id

-   zdrojové tabulky
    -   `municipalauthorities`
-   dodatečná transformace: Feature collection

#### /municipalauthorities/{id}/queues

-   zdrojové tabulky
    -   `skodapalace_queues`
