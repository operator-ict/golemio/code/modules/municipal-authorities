# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.3] - 2024-12-02

### Added

-   fix The entry point ./src is not included in the program for your provided tsconfig.

## [1.2.2] - 2024-12-02

### Added

-   asyncapi documentation ([ie#265](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/265))

## [1.2.1] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.0] - 2024-06-03

### Added

-   The `Cache-Control` header to all output gateway responses ([core#105](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/105))

### Removed

-   MongoDB remains

## [1.1.10] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.9] - 2024-04-08

### Fixed

-   API docs inconsistencies

### Changed

-   migration from axios to fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.1.8] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.1.7] - 2023-12-04

### Fixed

-   Fix and refactor history queue ([municipal-authorities#5](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/issues/5))

## [1.1.6] - 2023-11-27

### Changed

-   Replace bigint timestamps ([municipal-authorities#5](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/issues/5))

## [1.1.5] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.1.4] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.3] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.2] - 2023-05-29

### Changed

-   Update openapi docs

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.5] - 2022-11-03

### Changed

-   Mongo to PostgreSQL ([municipal-authorities#27](https://gitlab.com/operator-ict/golemio/code/modules/municipal-authorities/-/merge_requests/27))

## [1.0.4] - 2022-09-06

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))

## [1.0.3] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4
