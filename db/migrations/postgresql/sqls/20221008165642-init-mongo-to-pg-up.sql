CREATE TABLE IF NOT EXISTS municipalauthorities (
    "id" varchar NOT NULL,
	"geometry" geometry NOT NULL,
    "address_formatted" text,
    "agendas" json,
    "district" text ,
    "email" json,
    "image" json,
    "name" text NOT NULL,
    "official_board" text,
    "opening_hours" json,
    "telephone" json,
    "type" varchar(150),
    "description" varchar(150),
    "web" json,

    -- audit fields,
    create_batch_id int8,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id int8,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT municipalauthorities_pkey PRIMARY KEY ("id")
);

CREATE INDEX idx_municipalauthorities_geom ON municipalauthorities USING gist ("geometry");
CREATE INDEX municipalauthorities_district_idx ON municipalauthorities USING btree ("district");


CREATE TABLE IF NOT EXISTS skodapalace_queues (
    "municipal_authority_id" varchar(150) NOT NULL,
    "last_updated" bigint NOT NULL,
    "activity" varchar(150) NOT NULL,
    "number_of_person_in_queue" int8 NOT NULL,
    "number_of_serving_counters" int8 NOT NULL,
    "title" varchar(150) NOT NULL,

    -- audit fields,
    create_batch_id int8,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id int8,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT skodapalace_queues_pkey PRIMARY KEY ("activity")
);




CREATE TABLE IF NOT EXISTS skodapalace_queues_history (
    "municipal_authority_id" varchar(150) NOT NULL,
    "last_updated" bigint NOT NULL,
    "activity" varchar(150) NOT NULL,
    "number_of_person_in_queue" int8 NOT NULL,
    "number_of_serving_counters" int8 NOT NULL,

    -- audit fields,
    create_batch_id int8,
    created_at timestamptz,
    created_by varchar(150),
    update_batch_id int8,
    updated_at timestamptz,
    updated_by varchar(150),

    CONSTRAINT skodapalace_queues_history_pkey PRIMARY KEY ("activity", "last_updated")
);