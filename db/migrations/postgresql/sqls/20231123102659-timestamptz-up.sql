ALTER TABLE municipal_authorities.skodapalace_queues ALTER COLUMN "last_updated" TYPE timestamptz USING to_timestamp(last_updated / 1000.00);
ALTER TABLE municipal_authorities.skodapalace_queues_history ALTER COLUMN "last_updated" TYPE timestamptz USING to_timestamp(last_updated / 1000.00);
