ALTER TABLE municipal_authorities.skodapalace_queues ALTER COLUMN "last_updated" TYPE bigint using extract(epoch from last_updated) * 1000;
ALTER TABLE municipal_authorities.skodapalace_queues_history ALTER COLUMN "last_updated" TYPE bigint using extract(epoch from last_updated) * 1000;
