import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";

import { municipalAuthoritiesRouter } from "#og/MunicipalAuthoritiesRouter";
import { municipalAuthoritiesOutputDataFixture } from "../integration-engine/data/municipalauthorities-output";
import { municipalAuthoritiesQueuesOutputDataFixture } from "../integration-engine/data/municipalauthorities-queues-output";

chai.use(chaiAsPromised);

describe("MunicipalAuthorities Router", () => {
    const app = express();

    before(async () => {
        app.use("/municipalauthorities", municipalAuthoritiesRouter);

        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /municipalauthorities", (done) => {
        request(app)
            .get("/municipalauthorities")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should respond correctly to GET /municipalauthorities/:id", (done) => {
        request(app)
            .get("/municipalauthorities/urad-mestske-casti-praha-12")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.equal(municipalAuthoritiesOutputDataFixture);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /municipalauthorities/:id/queues", (done) => {
        request(app)
            .get("/municipalauthorities/skoduv-palac/queues")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });

    it("should respond correctly to GET /municipalauthorities/:id/queues", (done) => {
        request(app)
            .get("/municipalauthorities/skoduv-palac/queues")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.deep.equal(municipalAuthoritiesQueuesOutputDataFixture);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /municipalauthorities with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/municipalauthorities")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /municipalauthorities/:id with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/municipalauthorities/urad-mestske-casti-praha-12")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /municipalauthorities/:id/queues with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/municipalauthorities/skoduv-palac/queues")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });
});
