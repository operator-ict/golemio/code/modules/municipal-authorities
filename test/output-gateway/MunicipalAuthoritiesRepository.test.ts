import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { createSandbox, SinonSandbox } from "sinon";
import { MunicipalAuthoritiesRepository } from "#og";

chai.use(chaiAsPromised);

describe("MunicipalAuthoritiesRepository", () => {
    let sandbox: SinonSandbox;
    let municipalAuthoritiesRepository: MunicipalAuthoritiesRepository;

    before(() => {
        sandbox = createSandbox();
        municipalAuthoritiesRepository = new MunicipalAuthoritiesRepository();
    });

    after(() => {
        sandbox.restore();
    });

    it("should instantiate", () => {
        expect(municipalAuthoritiesRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await municipalAuthoritiesRepository.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.features).to.have.length(5);
    });

    it("should return single item", async () => {
        const id = "urad-mestske-casti-praha-12";
        const result = await municipalAuthoritiesRepository.GetOne(id);
        expect(result).not.to.be.empty;
        expect(result!.properties).to.have.property("id", id);
        expect(result!.properties).to.have.property("name", "Úřad městské části Praha 12");
    });
});
