import * as fs from "fs";
import { WaitingQueuesHelper } from "#ie/helpers/WaitingQueuesHelper";
import { expect } from "chai";

describe("WaitingQueuesHelper", () => {
    const helper = new WaitingQueuesHelper();
    const sourceData = JSON.parse(fs.readFileSync(__dirname + "/../data/skodapalacequeues-datasource.json").toString("utf8"));

    it("filters active queues correctly", () => {
        expect(helper.getActiveQueues(sourceData).length).to.eq(5);
    });

    it("gets correct last updated date", () => {
        expect(helper.getLastUpdatedDate(sourceData)).to.eql(new Date(1665479417000));
    });

    it("gets input queues for transformation correctly", () => {
        const input = helper.getActiveQueues(sourceData);
        const date = new Date(1665479417000);
        const result = helper.getInputQueuesForTransform(input, date);
        expect(result.length).to.eq(5);
        result.forEach((resultItem) => {
            expect(resultItem.lastUpdated).to.eql(date);
        });
    });

    it("return correct number of results from getActiveInputQueuesForTransform()", () => {
        expect(helper.getActiveInputQueuesForTransform(sourceData).length).to.eq(5);
    });
});
