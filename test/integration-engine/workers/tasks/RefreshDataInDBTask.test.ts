/* eslint-disable max-len */
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { RefreshDataInDBTask } from "#ie/workers/tasks";

import { IMunicipalAuthority } from "#sch/MunicipalAuthorities";
import { IMunicipalAuthoritiesInput } from "#sch/datasources/MunicipalAuthoritiesJsonSchema";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    let testData: IMunicipalAuthoritiesInput[];
    let testTransformedData: IMunicipalAuthority[];

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        task = new RefreshDataInDBTask("test.municipalauthorities");

        testData = [
            {
                name: "Magistrát hlavního města Prahy",
                slug: "magistrat-hlavniho-mesta-prahy",
                address: "Mariánské náměstí 2/2, 110 01 Praha 1",
                district: "praha-1",
                coordinates: [14.4178354, 50.0871095],
                opening_hours_monday: "12:00 - 17:00",
                opening_hours_wednesday: "08:00 - 18:00",
                official_board: "http://www.praha.eu/jnp/cz/o_meste/magistrat/deska/index.html",
                agendas: [
                    {
                        description: "Pronájem bytových a nebytových prostor",
                        long_description: "",
                        keywords: ["pronájem", "byt", "pronájem bytu", "nebytové prostory"],
                    },
                ],
                image: {
                    filename: "VJXPN4RVrTLnkih5.png",
                    size: 714603,
                    mimetype: "image/png",
                    url: "/images/municipal-authorities/VJXPN4RVrTLnkih5.png",
                },
                type: "city-hall",
                web: ["www.praha.eu/"],
                email: ["posta@praha.eu"],
                telephone: ["12 444"],
            },
        ];

        testTransformedData = [
            {
                id: "magistrat-hlavniho-mesta-prahy",
                geometry: { coordinates: [14.4178354, 50.0871095], type: "Point" },
                name: "Magistrát hlavního města Prahy",
                address_formatted: "Mariánské náměstí 2/2, 110 01 Praha 1",
                telephone: ["12 444"],
                web: ["www.praha.eu/"],
                email: ["posta@praha.eu"],
                district: "praha-1",
                official_board: "http://www.praha.eu/jnp/cz/o_meste/magistrat/deska/index.html",
                image: {
                    mimetype: "image/png",
                    size: 714603,
                    url: "https://www.mojepraha.eu/images/municipal-authorities/VJXPN4RVrTLnkih5.png",
                },
                type: "city-hall",
                description: "Magistrát",
                opening_hours: [
                    { closes: "17:00", day_of_week: "Monday", description: null, opens: "12:00" },
                    { closes: "18:00", day_of_week: "Wednesday", description: null, opens: "08:00" },
                ],
                agendas: [
                    {
                        keywords: ["pronájem", "byt", "pronájem bytu", "nebytové prostory"],
                        description: "Pronájem bytových a nebytových prostor",
                    },
                ],
            },
        ];

        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(task["repository"], "saveBulk").callsFake(() => Promise.resolve());
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();

        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].saveBulk as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveBulk as SinonSpy, testTransformedData);

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["transformation"].transform as SinonSpy,
            task["repository"].saveBulk as SinonSpy
        );
    });
});
