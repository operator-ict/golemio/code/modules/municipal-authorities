import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { SaveWaitingQueuesDataToHistoryTask } from "#ie/workers/tasks";

import { ISkodaPalaceQueueHistory, ISkodaPalaceQueueMessage } from "#sch/SkodaPalaceQueues";

describe("SaveWaitingQueuesDataToHistoryTask", () => {
    let sandbox: SinonSandbox;
    let task: SaveWaitingQueuesDataToHistoryTask;

    const testData: ISkodaPalaceQueueMessage = {
        last_updated: new Date(1665479417000).toISOString(),
        municipal_authority_id: "skoduv-palac",
        activity: "Lítačka - vyhotovení karty",
        number_of_person_in_queue: 0,
        number_of_serving_counters: 12,
        title: "Monitoring odbavování klientů ve Škodově paláci",
    };

    const testTransformedData: ISkodaPalaceQueueHistory = {
        last_updated: new Date(1665479417000),
        municipal_authority_id: "skoduv-palac",
        activity: "Lítačka - vyhotovení karty",
        number_of_person_in_queue: 0,
        number_of_serving_counters: 12,
    };

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        task = new SaveWaitingQueuesDataToHistoryTask("test.municipalauthorities");

        sandbox.stub(task["transformation"], "transformElement").callsFake(() => testTransformedData);
        sandbox.stub(task["repository"], "save").callsFake(() => Promise.resolve());
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"](testData);

        sandbox.assert.calledOnce(task["transformation"].transformElement as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transformElement as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].save as SinonSpy);
        sandbox.assert.calledWith(task["repository"].save as SinonSpy, testTransformedData);

        sandbox.assert.callOrder(task["transformation"].transformElement as SinonSpy, task["repository"].save as SinonSpy);
    });
});
