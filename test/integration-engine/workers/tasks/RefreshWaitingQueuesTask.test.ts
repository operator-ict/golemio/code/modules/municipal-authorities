/* eslint-disable max-len */
import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { RefreshWaitingQueuesTask } from "#ie/workers/tasks";

import { IInputQueueForTransform, ISkodaPalaceQueue } from "#sch/SkodaPalaceQueues";
import { ISkodaPalaceQueuesInput } from "#sch/datasources/SkodaPalaceQueuesJsonSchema";

describe("RefreshWaitingQueuesTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshWaitingQueuesTask;
    let testData: ISkodaPalaceQueuesInput = {
        return: [
            {
                cinnostID: 5515,
                nazev: "Lítačka - vyhotovení karty",
                fronta: 0,
                pocet_prepazek: 12,
                doba_cekani_odhad: 0,
                m_poradi_aktivni: 0,
                m_aktivni: 1,
                doba_cekani: 0,
            },
        ],
        apiInfo: { currentTime: 1665479417, apiVersion: "10.0.3", execTime: 0.009260892868041992, usageMemory: 1374248 },
    };

    let preparedData: IInputQueueForTransform[] = [
        {
            lastUpdated: new Date(1665479417000),
            inputQueue: {
                cinnostID: 5515,
                nazev: "Lítačka - vyhotovení karty",
                fronta: 0,
                pocet_prepazek: 12,
                doba_cekani_odhad: 0,
                m_poradi_aktivni: 0,
                m_aktivni: 1,
                doba_cekani: 0,
            },
        },
    ];

    let testTransformedData: ISkodaPalaceQueue[] = [
        {
            last_updated: new Date(1665479417000),
            municipal_authority_id: "skoduv-palac",
            activity: "Lítačka - vyhotovení karty",
            number_of_person_in_queue: 0,
            number_of_serving_counters: 12,
            title: "Monitoring odbavování klientů ve Škodově paláci",
        },
    ];

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        task = new RefreshWaitingQueuesTask("test.municipalauthorities");

        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["helper"], "getActiveInputQueuesForTransform").callsFake(() => preparedData);
        sandbox.stub(task["transformation"], "transformArray").callsFake(() => testTransformedData);
        sandbox.stub(task["repository"], "saveBulk").callsFake(() => Promise.resolve());
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call correct methods", async () => {
        await task["execute"]();

        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["helper"].getActiveInputQueuesForTransform as SinonSpy);
        sandbox.assert.calledWith(task["helper"].getActiveInputQueuesForTransform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["transformation"].transformArray as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].saveBulk as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveBulk as SinonSpy, testTransformedData);
        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 1);

        for (const data of testTransformedData) {
            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.municipalauthorities",
                "saveWaitingQueuesDataToHistory",
                data
            );
        }

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["helper"].getActiveInputQueuesForTransform as SinonSpy,
            task["transformation"].transformArray as SinonSpy,
            task["repository"].saveBulk as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});
