import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";

import { MunicipalAuthoritiesWorker } from "#ie/workers/MunicipalAuthoritiesWorker";

describe("MunicipalAuthoritiesWorker", () => {
    let sandbox: SinonSandbox;
    let worker: MunicipalAuthoritiesWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        worker = new MunicipalAuthoritiesWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".municipalauthorities");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("MunicipalAuthorities");
            expect(result.queues.length).to.equal(3);
        });
    });

    describe("registerTask", () => {
        it("should have 3 tasks registered", () => {
            expect(worker["queues"].length).to.equal(3);
            expect(worker["queues"][0].name).to.equal("refreshDataInDB");
            expect(worker["queues"][1].name).to.equal("refreshWaitingQueues");
            expect(worker["queues"][2].name).to.equal("saveWaitingQueuesDataToHistory");
            expect(worker["queues"][0].options.messageTtl).to.equal(82800000);
            expect(worker["queues"][1].options.messageTtl).to.equal(60000);
        });
    });
});
