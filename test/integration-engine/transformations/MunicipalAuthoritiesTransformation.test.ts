import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { MunicipalAuthoritiesTransformation } from "#ie/transformations/MunicipalAuthoritiesTransformation";

chai.use(chaiAsPromised);

describe("MunicipalAuthoritiesTransformation", () => {
    let transformation: MunicipalAuthoritiesTransformation;
    let testSourceData: any[];

    const transformedDataFixture = {
        id: "magistrat-hlavniho-mesta-prahy",
        geometry: { coordinates: [14.4178354, 50.0871095], type: "Point" },
        name: "Magistrát hlavního města Prahy",
        address_formatted: "Mariánské náměstí 2/2, 110 01 Praha 1",
        telephone: ["12 444", "236 002 428", "236 002 376"],
        web: ["www.praha.eu/"],
        email: ["posta@praha.eu"],
        district: "praha-1",
        official_board: "http://www.praha.eu/jnp/cz/o_meste/magistrat/deska/index.html",
        image: {
            mimetype: "image/png",
            size: 714603,
            url: "undefined/images/municipal-authorities/VJXPN4RVrTLnkih5.png",
        },
        type: "city-hall",
        description: "Magistrát",
        opening_hours: [
            {
                closes: "17:00",
                day_of_week: "Monday",
                description: null,
                opens: "12:00",
            },
            {
                closes: "18:00",
                day_of_week: "Wednesday",
                description: null,
                opens: "08:00",
            },
        ],
        agendas: [
            {
                keywords: ["pronájem", "byt", "pronájem bytu", "nebytové prostory"],
                description: "Pronájem bytových a nebytových prostor",
            },
        ],
    };

    beforeEach(async () => {
        transformation = new MunicipalAuthoritiesTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/municipalauthorities-datasource.json", "utf8"));
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MunicipalAuthorities");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = await transformation["transformElement"](testSourceData[0]);
        expect(data).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (const item of data) {
            expect(item).to.have.property("geometry");
            expect(item).to.have.property("id");
            expect(item).to.have.property("name");
        }
    });
});
