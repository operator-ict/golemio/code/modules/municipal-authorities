import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { SkodaPalaceQueuesTransformation } from "#ie/transformations/SkodaPalaceQueuesTransformation";
import { IInputQueueForTransform } from "#sch/SkodaPalaceQueues";

chai.use(chaiAsPromised);

describe("SkodaPalaceQueuesTransformation", () => {
    const transformation = new SkodaPalaceQueuesTransformation();

    const transformedDataFixture = {
        activity: "Lítačka - vyhotovení karty",
        last_updated: new Date(1665479417000),
        municipal_authority_id: "skoduv-palac",
        number_of_person_in_queue: 0,
        number_of_serving_counters: 3,
        title: "Monitoring odbavování klientů ve Škodově paláci",
    };

    const testSourceData: IInputQueueForTransform = {
        lastUpdated: new Date(1665479417000),
        inputQueue: {
            cinnostID: 5499,
            nazev: "Lítačka - vyhotovení karty",
            fronta: 0,
            pocet_prepazek: 3,
            doba_cekani_odhad: 0,
            m_poradi_aktivni: 0,
            m_aktivni: 0,
            doba_cekani: 0,
        },
    };

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("SkodaPalaceQueuesTransformation");
    });

    it("should correctly transform element", async () => {
        const data = await transformation.transformElement(testSourceData);
        expect(data).to.deep.equal(transformedDataFixture);
    });
});
