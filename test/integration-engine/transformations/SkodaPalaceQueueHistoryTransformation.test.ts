import { SkodaPalaceQueueHistoryTransformation } from "#ie/transformations/SkodaPalaceQueueHistoryTransformation";
import { expect } from "chai";

describe("SkodaPalaceQueueHistoryTransformation", () => {
    const transformation = new SkodaPalaceQueueHistoryTransformation();

    it("transforms source message correctly", () => {
        const messgage = {
            last_updated: "2023-11-27T12:58:03.000Z",
            municipal_authority_id: "skoduv-palac",
            activity: "Lítačka - výdej již hotové karty",
            number_of_person_in_queue: 0,
            number_of_serving_counters: 11,
            title: "Monitoring odbavování klientů ve Škodově paláci",
        };

        const result = transformation.transformElement(messgage);
        expect(result).to.eql({
            activity: "Lítačka - výdej již hotové karty",
            last_updated: new Date(1701089883000),
            municipal_authority_id: "skoduv-palac",
            number_of_person_in_queue: 0,
            number_of_serving_counters: 11,
        });
    });
});
