export const municipalAuthoritiesOutputDataFixture = {
    geometry: { coordinates: [14.4132043, 50.0017822], type: "Point" },
    properties: {
        id: "urad-mestske-casti-praha-12",
        agendas: [
            {
                keywords: ["pronájem", "byt", "pronájem bytu", "nebytové prostory"],
                description: "Pronájem bytových a nebytových prostor",
            },
            { keywords: ["konverze", "dokumenty", "elektronická", "převedení"], description: "Autorizovaná konverze dokumentů" },
            {
                keywords: ["podpis", "ověření", "kopie", "listina"],
                long_description: "Ověřování shody opisu nebo kopie s listinou a ověřování pravosti podpisu",
                description: "Ověřování shody opisu a ověřování podpisů",
            },
            { keywords: [], description: "Přijímá návrhy na vyhlášení místního referenda" },
            { keywords: [], description: "Přijímá podněty k instalování kamerového systému" },
            {
                keywords: [],
                long_description: "Přijímá přihlášení k trvalému pobytu a ohlášení změny místa trvalého pobytu",
                description: "Přijímá přihlášení k trvalému pobytu",
            },
            { keywords: ["psi", "pes", "hafan", "čokl", "psék", "smeták", "voříšek"], description: "Vybírá poplatky ze psů" },
            { keywords: ["volit", "volby"], description: "Vydává voličský průkaz" },
            { keywords: [], description: "Vydává výpis z evidence Rejstříku trestů" },
            { keywords: ["body", "řidičák", "řidičský průkaz"], description: "Vydává výpisy z bodového hodnocení řidiče" },
            { keywords: [], description: "Vydává výpisy z insolvenčního rejstříku" },
            { keywords: [], description: "Vydává výpisy z katastru nemovitostí" },
            {
                keywords: [],
                long_description: "Přijímá nalezené občanské průkazy a další potvrzení, občanské průkazy zemřelého",
                description: "Přijímá nalezené občanské průkazy",
            },
            { keywords: [], description: "Přijímá žádosti o vydání cestovního dokladu" },
            { keywords: ["občanka", "občanský"], description: "Přijímá žádosti o vydání občanského průkazu" },
            { keywords: [], description: "Přijímá žádosti o výdej údajů z evidence obyvatel" },
            { keywords: [], description: "Přijímá žádosti o zprostředkování kontaktu" },
            { keywords: [], description: "Vydává cestovní doklad" },
            {
                keywords: ["obcanka", "občanka", "občanku", "obcanku", "OP", "občanky", "obcanky"],
                description: "Vydává občanský průkaz",
            },
            { keywords: [], description: "Vydává údaje z evidence obyvatel" },
            { keywords: [], description: "Přijímá doklady k uzavření manželství" },
            {
                keywords: [],
                long_description: "Přijímá nalezené občanské průkazy a další potvrzení, občanské průkazy zemřelého",
                description: "Přijímá nalezené občanské průkazy",
            },
            {
                keywords: [],
                long_description: "Přijímá oznámení o narození dítěte a provádí zápis do knihy narození",
                description: "Přijímá oznámení o narození dítěte",
            },
            {
                keywords: [],
                long_description: "Přijímá oznámení o úmrtí a provádí zápis do knihy úmrtí",
                description: "Přijímá oznámení o úmrtí",
            },
            { keywords: [], description: "Přijímá žádosti o udělení státního občanství České republiky" },
            { keywords: [], description: "Přijímá žádosti o uzavření manželství" },
            {
                keywords: ["rodný list", "úmrtní list", "oddací list"],
                description: "Přijímá žádosti o vydání matričních dokladů (rodného, oddacího, úmrtního listu)",
            },
            { keywords: ["trestní", "rejstřík"], description: "Přijímá žádosti o vydání výpisu z evidence Rejstříku trestů" },
            { keywords: [], description: "Přijímá žádosti o změnu jména nebo příjmení" },
            { keywords: [], description: "Přijímá žádosti o zprostředkování kontaktu" },
            { keywords: ["manželství", "svatba", "sňatek"], description: "Vydává oddací listy" },
            { keywords: [], description: "Vydává rodné listy" },
            { keywords: ["úmrtí", "smrt", "zemřelý"], description: "Vydává úmrtní listy" },
            { keywords: ["stavební", "budova", "stržení", "demolice"], description: "Nařizuje odstranění stavby" },
            { keywords: ["pasport", "stavba", "budova"], description: "Zjednodušené projektové dokumentace stavby" },
            {
                keywords: [
                    "stavební",
                    "stavba",
                    "závady",
                    "bezpečnost",
                    "zdraví",
                    "hygiena",
                    "konzervace",
                    "bezbarierový",
                    "ochrana",
                ],
                description: "Nařizuje provádění nezbytných úprav",
            },
            { keywords: [], long_description: "Nařizuje provádění udržovacích prací", description: "Udržovací práce" },
            { keywords: ["ohrožení", "zdraví", "zabezpečení", "nebezpečí"], description: "Neodkladné zabezpečovací práce" },
            {
                keywords: ["stavba"],
                long_description: "Oznamuje stavební povolení a rozhodnutí o prodloužení platnosti stavebního povolení",
                description: "Stavební povolení",
            },
            {
                keywords: ["stavební", "stavba", "budova"],
                long_description: "Oznamuje zahájení stavebního řízení, nařizuje ústní jednání a místní šetření",
                description: "Oznamuje zahájení stavebního řízení",
            },
            {
                keywords: ["reklama", "propagace", "povolení"],
                long_description:
                    "Přijímá ohlášení na informační, reklamní a propagační zařízení, vydává povolení na jeho umístění",
                description: "Přijímá ohlášení na reklamní a propagační zařízení",
            },
            {
                keywords: ["stavba", "předčasné užívání"],
                long_description: "Povolení k předčasnému užívání stavby před dokončením",
                description: "Povolení k předčasnému užívání stavby",
            },
            { keywords: [], description: "Přijímá žádosti o kolaudační souhlas" },
            { keywords: [], description: "Přijímá žádosti o stavební povolení" },
            { keywords: [], description: "Vede stavební řízení" },
            { keywords: ["kolaudace"], description: "Vydává kolaudační souhlas" },
            { keywords: ["stavba", "budova"], description: "Vydává stavební povolení" },
            { keywords: [], description: "Vydává územní rozhodnutí" },
            {
                keywords: ["tabak", "alkohol", "lihoviny", "cigarety", "doutniky"],
                description: "Kontrola značení tabákových výrobků a lihovin",
            },
            {
                keywords: [],
                long_description: "Provádí dozor nad dodržováním zákona o ochraně spotřebitele",
                description: "Dodržování zákona o ochraně spotřebitele",
            },
            { keywords: [], description: "Provádí zápisy do živnostenského rejstříku" },
            { keywords: ["podnikání"], description: "Přijímá ohlášení vázané živnosti" },
            { keywords: ["podnikání"], description: "Přijímá ohlášení volné živnosti" },
            { keywords: ["podnikání"], description: "Přijímá ohlášení řemeslné živnosti" },
            { keywords: ["koncese"], description: "Přijímá žádosti o koncesi" },
            { keywords: ["koncese", "právník", "právnická koncese"], description: "Žádosti o koncesi pro právnické osoby" },
            { keywords: [], description: "Přijímá žádosti o zrušení živnostenského oprávnění" },
            { keywords: [], description: "Vydává výpisy ze živnostenského rejstříku" },
        ],
        district: "praha-12",
        email: ["informace@praha12.cz"],
        image: {
            mimetype: "image/png",
            size: 522370,
            url: "https://www.mojepraha.eu/images/municipal-authorities/ozh7pRLOEeI-Zfsv.png",
        },
        name: "Úřad městské části Praha 12",
        official_board: null,
        opening_hours: [
            { closes: "12:00", day_of_week: "Monday", description: null, opens: "08:00" },
            { closes: "18:00", day_of_week: "Monday", description: null, opens: "13:00" },
            { closes: "19:00", day_of_week: "Monday", description: "ev. obyvatel a os. dokladů", opens: "08:00" },
            { closes: "17:45", day_of_week: "Monday", description: "pokl. hod.", opens: "08:00" },
            { closes: "19:00", day_of_week: "Monday", description: "kopírovací služba", opens: "07:00" },
            { closes: "12:00", day_of_week: "Tuesday", description: "ev. obyvatel a os. dokladů", opens: "08:00" },
            { closes: "15:45", day_of_week: "Tuesday", description: "pokl. hod.", opens: "08:00" },
            { closes: "16:00", day_of_week: "Tuesday", description: "kopírovací služba", opens: "07:00" },
            { closes: "12:00", day_of_week: "Wednesday", description: null, opens: "08:00" },
            { closes: "18:00", day_of_week: "Wednesday", description: null, opens: "13:00" },
            { closes: "19:00", day_of_week: "Wednesday", description: "ev. obyvatel a os. dokladů", opens: "08:00" },
            { closes: "17:45", day_of_week: "Wednesday", description: "pokl. hod.", opens: "08:00" },
            { closes: "19:00", day_of_week: "Wednesday", description: "kopírovací služba", opens: "07:00" },
            { closes: "12:00", day_of_week: "Thursday", description: "ev. obyvatel a os. dokladů", opens: "08:00" },
            { closes: "15:45", day_of_week: "Thursday", description: "pokl. hod.", opens: "08:00" },
            { closes: "16:00", day_of_week: "Thursday", description: "kopírovací služba", opens: "07:00" },
            { closes: "12:00", day_of_week: "Friday", description: "pokl. hod.", opens: "08:00" },
            { closes: "14:00", day_of_week: "Friday", description: "kopírovací služba", opens: "07:00" },
        ],
        telephone: ["244 028 111", "244 028 256", "241 763 173", "241 763 172"],
        web: ["www.praha12.cz/"],
        updated_at: "2022-10-11T10:48:28.547Z",
        address: { address_formatted: "Písková 830/25, 143 00 Praha 4" },
        type: { description: "Obecní úřad", id: "municipality" },
    },
    type: "Feature",
};
