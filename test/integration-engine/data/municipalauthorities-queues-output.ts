export const municipalAuthoritiesQueuesOutputDataFixture = {
    last_updated: "2022-10-11T11:10:17.000+02:00",
    municipal_authority_id: "skoduv-palac",
    served_activities: [
        {
            activity: "Lítačka - vyhotovení karty",
            number_of_person_in_queue: 0,
            number_of_serving_counters: 12,
        },
        {
            activity: "Lítačka - dobití kupónu",
            number_of_person_in_queue: 1,
            number_of_serving_counters: 11,
        },
        {
            activity: "Lítačka - objednaní klienti",
            number_of_person_in_queue: 0,
            number_of_serving_counters: 11,
        },
        {
            activity: "Lítačka - výdej již hotové karty",
            number_of_person_in_queue: 0,
            number_of_serving_counters: 11,
        },
        {
            activity: "tam",
            number_of_person_in_queue: 0,
            number_of_serving_counters: 0,
        },
    ],
    updated_at: "2022-10-11T13:10:17.578+02:00",
    title: "Monitoring odbavování klientů ve Škodově paláci",
};
